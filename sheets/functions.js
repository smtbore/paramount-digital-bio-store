const body = document.querySelector(".page");
const section = document.querySelector(".nav-bar");
const button = document.querySelector(".nav-button");
// constants containing elements obtained through their class names

const navItems = section.querySelectorAll("nav a");
// constant containing all items in the nav and a tags within the class selected in section constant

const array = Array.apply(null, navItems);
// array stored within a constant containing the values from navItems const

body.addEventListener("transitioned", removeMenu, false);
button.addEventListener("click", sectionMenu, false);
body.addEventListener(
  "click",
  function (e) {
    clickTarget(e);
  },
  true
);
body.addEventListener("focus", catchFocus, true);
// event listeners for elements in the HTML

// Toggle section class on body element, set aria-expanded and screen reader text on button:
function sectionMenu() {
  body.classList.toggle("section");
  section.classList.add("open");
}

// Hide nav area when focus shifts away:
function catchFocus() {
  if (
    !(
      array.includes(document.activeElement) ||
      document.activeElement === button
    )
  ) {
    sectionMenu();
  } else {
    return;
  }
}

// Hide nav area when touch or click happens elsewhere:
function clickTarget(e) {
  if (!section.contains(e.target)) {
    sectionMenu();
  }
}

// Liten for clicks on button button:

// Listen for focus changes:

// Listen for clicks:
